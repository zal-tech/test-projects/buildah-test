# Variables
variables:
  # Job Images
  BUILDAH_IMAGE: quay.io/buildah/stable:v1.23.3
  CST_IMAGE: registry.gitlab.com/zal-tech/test-projects/prototyping/build-agents/cst-agent:latest
  DIND_IMAGE: docker:20.10.17-dind
  DIVE_IMAGE: wagoodman/dive:v0.10.0
  DOCKER_IMAGE: docker:20.10.17
  DOCKER_SLIM_IMAGE: registry.gitlab.com/zal-tech/test-projects/prototyping/build-agents/docker-slim-agent:latest
  HADOLINT_IMAGE: hadolint/hadolint:v2.9.2-alpine
  SEMANTIC_RELEASE_IMAGE: registry.gitlab.com/zal-tech/test-projects/prototyping/build-agents/semantic-release-agent:latest
  SKOPEO_IMAGE: quay.io/skopeo/stable:v1.9.2
  SNYK_IMAGE: snyk/snyk:alpine
  TRIVY_IMAGE: aquasec/trivy:0.30.4

  # Pipeline Vars
  IMAGE_REPO: $CI_REGISTRY_IMAGE
  IMAGE_TAR: $CI_PROJECT_NAME.tar
  DEV_IMAGE: $IMAGE_REPO:$CI_COMMIT_SHA

  # Pipeline Options (Can be overwritten in projects)
  CONTAINER_CONTEXT: .
  CONTAINER_FILE: Containerfile
  CST_FILE: test/container-structure-test.yml
  DEV_RELEASES: "true"
  SEMANTIC_RELEASE: "false"
  SLIM_BUILD: "false"
  TAG_LATEST: "true"

# Workflow
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# Stages
stages:
  - container-analyze
  - container-build
  - container-test
  - container-scan
  - container-release

# References
.buildah-rules:
  # ToDo: Change this to 'release-type-basic'?
  release-type-basic:
    - if: $SEMANTIC_RELEASE != "false"
      when: never
  # ToDo: Rename this to 'enable-dev-releases'?
  enable-dev-releases:
    - if: $DEV_RELEASES != "true"
      when: never
  # ToDo: Change this to 'release-type-semantic'?
  release-type-semantic:
    - if: $SEMANTIC_RELEASE != "true"
      when: never
  slim-build:
    - if: $SLIM_BUILD != "true"
      when: never
  # ToDo: Rename this to 'enable-snyk-jobs'?
  snyk:
    - if: $SNYK_TOKEN

.buildah-scripts:
  buildah-auth:
    - echo "$REGISTRY_PASSWORD" | buildah login -u $REGISTRY_USER --password-stdin $REGISTRY_URL
  skopeo-auth:
    - echo "$REGISTRY_PASSWORD" | skopeo login -u $REGISTRY_USER --password-stdin $REGISTRY_URL
  snyk-auth:
    - snyk auth $SNYK_TOKEN

.buildah-caches:
  container-read:
    key: ContainerCache-$CI_COMMIT_SHA
    paths:
      - $IMAGE_TAR
    policy: pull
  container-write:
    key: ContainerCache-$CI_COMMIT_SHA
    paths:
      - $IMAGE_TAR
    policy: push

# Analysis Jobs
hadolint:
  stage: container-analyze
  image:
    name: $HADOLINT_IMAGE
    entrypoint: [""]
  needs: []
  script:
    - hadolint $CONTAINER_CONTEXT/$CONTAINER_FILE

trivy-confg:
  stage: container-analyze
  image:
    name: $TRIVY_IMAGE
    entrypoint: [""]
  needs: []
  script:
    # Note: This will include all types of files trivy can scan and not just the Containerfile if they are present.
    - trivy config . # Print out results in table format.
    - trivy config -f sarif . > trivy-config-results.json # Save results to file in sarif/json format.
    - trivy config --exit-code 1 --severity CRITICAL . # Fail job on critical vulnerabilities.
  artifacts:
    when: always
    paths:
      - trivy-config-results.json

# Build Jobs
buildah:
  stage: container-build
  image:
    name: $BUILDAH_IMAGE
    entrypoint: [""]
  needs: []
  variables:
    REGISTRY_USER: gitlab-ci-token
    REGISTRY_PASSWORD: $CI_JOB_TOKEN
    REGISTRY_URL: $CI_REGISTRY
  before_script:
    - !reference [.buildah-scripts, buildah-auth]
  script:
    - buildah pull $IMAGE_REPO:latest || true
    - buildah build --file $CONTAINER_CONTEXT/$CONTAINER_FILE --cache-from $IMAGE_REPO:latest --tag $DEV_IMAGE $CONTAINER_CONTEXT
    - buildah push $DEV_IMAGE docker-archive:$IMAGE_TAR:$DEV_IMAGE
  cache:
    - !reference [.buildah-caches, container-write]

slim-image:
  stage: container-build
  image: $DOCKER_SLIM_IMAGE
  services:
    - $DIND_IMAGE # [Future] docker-slim doesn't full support podman yet.
  needs:
    - job: buildah
  rules:
    - !reference [.buildah-rules, slim-build ]
    - if: $CI_COMMIT_BRANCH
  variables:
    DOCKER_SLIM_OPTS: --http-probe=false
    DOCKER_SLIM_EXEC_FILE: $CONTAINER_CONTEXT/docker-slim-exec.sh
    DOCKER_SLIM_CMD: --continue-after exec --exec-file $DOCKER_SLIM_EXEC_FILE
  script:
    - docker load --input $IMAGE_TAR
    - docker-slim --in-container --report ./docker-slim-results/slim-report.json build
      --show-blogs --show-clogs --tag $DEV_IMAGE  --copy-meta-artifacts ./docker-slim-results --include-shell --sensor-ipc-mode proxy
      $DOCKER_SLIM_OPTS $DOCKER_SLIM_CMD --target $DEV_IMAGE
    - docker save --output $IMAGE_TAR $DEV_IMAGE
  cache:
    - !reference [ .buildah-caches, container-read ]
    - !reference [ .buildah-caches, container-write ]
  artifacts:
    when: always
    paths:
      - docker-slim-results/

# Test Jobs
container-structure-test:
  stage: container-test
  image: $CST_IMAGE
  services:
    - $DIND_IMAGE # [Future] Container-Structure-Test isn't fully supported by podman yet
  needs:
    - job: buildah
    - job: slim-image
      optional: true
  script:
    - docker load --input $IMAGE_TAR
    - container-structure-test test --image $DEV_IMAGE --config $CST_FILE -o junit --test-report container-test.xml
  cache:
    - !reference [.buildah-caches, container-read]
  artifacts:
    when: always
    paths:
      - container-test.xml
    reports:
      junit: container-test.xml

# Scan Jobs
snyk-test:
  stage: container-scan
  image:
    name: $SNYK_IMAGE
    entrypoint: [ "" ]
  rules:
    - !reference [ .buildah-rules, snyk ]
  needs:
    - job: buildah
    - job: slim-image
      optional: true
  before_script:
    - !reference [ .buildah-scripts, snyk-auth ]
  script:
    - snyk container test docker-archive:$IMAGE_TAR --project-name=$CI_PROJECT_PATH
      --file=$CONTAINER_CONTEXT/$CONTAINER_FILE --sarif-file-output=snyk_results.json
      --app-vulns --exclude-base-image-vulns --fail-on=all
  cache:
    - !reference [.buildah-caches, container-read]
  artifacts:
    when: always
    paths:
      - snyk_results.json

snyk-monitor:
  stage: container-scan
  image:
    name: $SNYK_IMAGE
    entrypoint: [ "" ]
  rules:
    - !reference [ .buildah-rules, snyk]
  needs:
    - job: snyk-test
  before_script:
    - !reference [ .buildah-scripts, snyk-auth ]
  script:
    - snyk container monitor docker-archive:$IMAGE_TAR --project-name=$CI_PROJECT_PATH
      --file=$CONTAINER_CONTEXT/$CONTAINER_FILE
      --app-vulns --exclude-base-image-vulns --target-reference="$CI_COMMIT_BRANCH"
  cache:
    - !reference [.buildah-caches, container-read]

trivy-scan:
  stage: container-scan
  image:
    name: $TRIVY_IMAGE
    entrypoint: [""]
  needs:
    - job: buildah
    - job: slim-image
      optional: true
  script:
    - trivy image --input $IMAGE_TAR # Print out results in table format.
    - trivy image --input $IMAGE_TAR -f sarif > trivy-scan-results.json # Save results to file in sarif/json format.
    - trivy image --input $IMAGE_TAR --exit-code 1 --severity CRITICAL . # Fail job on critical vulnerabilities.
  cache:
    - !reference [ .buildah-caches, container-read ]
  artifacts:
    when: always
    paths:
      - trivy-scan-results.json

dive-scan:
  stage: container-scan
  image:
    name: $DIVE_IMAGE
    entrypoint: [""]
  needs:
    - job: buildah
    - job: slim-image
      optional: true
  script:
    - CI=true dive docker-archive://$IMAGE_TAR | tee dive-results.txt
  cache:
    - !reference [ .buildah-caches, container-read ]
  artifacts:
    when: always
    paths:
      - dive-results.txt

# Release Jobs
.image-release-gate:
  stage: container-release
  needs:
    # Build Jobs
    - job: buildah
      artifacts: false
    - job: slim-image
      artifacts: false
      optional: true
    # Test/Scan/etc. Jobs (set to optional to allow for pipeline/project specifics)
    - job: hadolint
      artifacts: false
      optional: true
    - job: trivy-config
      artifacts: false
      optional: true
    - job: container-structure-test
      artifacts: false
      optional: true
    - job: snyk-test
      artifacts: false
      optional: true
    - job: trivy-scan
      artifacts: false
      optional: true
    - job: dive-scan
      artifacts: false
      optional: true

  # Note: This anchor and the semantic, basic, and dev release jobs should be defined in their own job template (common/release-control?)
  # (sem/basic release jobs will extend the release-base, but the final jobs in the pipeline will extend sem/basic anchors + release gate)
.release-base:
  image: $SEMANTIC_RELEASE_IMAGE
  artifacts:
    when: always
    reports:
      dotenv: build.env

semantic-release:
  extends:
    - .release-base
    - .image-release-gate
  rules:
    - !reference [ .buildah-rules, release-type-semantic ]
    # Additional rules to be set to determine when dev vs release
    - if: $CI_COMMIT_BRANCH
  variables:
    GITLAB_TOKEN: $RELEASE_TOKEN
  script:
    # Note: semantic-release MUST export the new version to the 'RELEASE_VERSION' env var.
    - semantic-release
    - echo IMAGE_VERSION=$RELEASE_VERSION > build.env
    - 'echo Image Version: $IMAGE_VERSION'
  resource_group: container-release

basic-release:
  extends:
    - .release-base
    - .image-release-gate
  rules:
    - !reference [ .buildah-rules, release-type-basic ]
    # Additional rules to be set to determine when dev vs release
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    # Override the script section if you need a custom means to detect/update the release version.
    - export IMAGE_VERSION=$(jq -r '.version' project.json)
    - echo IMAGE_VERSION=$IMAGE_VERSION > build.env
    - 'echo Image Version: $IMAGE_VERSION'
  resource_group: container-release

dev-release:
  # Acts as a gate for dev-releases and allows for setting the dev-release tag.
  extends:
    - .release-base
    - .image-release-gate
  rules:
    - !reference [ .buildah-rules, enable-dev-releases ]
    # Additional rules to be set to determine when dev vs release
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
  variables:
    DEV_RELEASE_TAG: $CI_COMMIT_BRANCH-$CI_COMMIT_SHORT_SHA
  script:
    - echo IMAGE_VERSION=$DEV_RELEASE_TAG > build.env

.push-image:
  stage: container-release
  image:
    name: $SKOPEO_IMAGE
    entrypoint: [ "" ]
  needs:
    - job: semantic-release
      optional: true
    - job: basic-release
      optional: true
  before_script:
    - !reference [ .buildah-scripts, skopeo-auth ]
  script:
    - skopeo copy docker-archive:$IMAGE_TAR docker://$IMAGE_REPO:$IMAGE_VERSION
    - |
      if [[ $TAG_LATEST == "true" ]]; then
        skopeo copy docker-archive:$IMAGE_TAR docker://$IMAGE_REPO:latest      
      fi
  cache:
    - !reference [ .buildah-caches, container-read ]

push-image-to-gitlab:
  extends: .push-image
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  variables:
    IMAGE_REPO: $CI_REGISTRY_IMAGE
    REGISTRY_USER: gitlab-ci-token
    REGISTRY_PASSWORD: $CI_JOB_TOKEN
    REGISTRY_URL: $CI_REGISTRY

push-dev-image-to-gitlab:
  extends: push-image-to-gitlab
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
  needs:
    - job: dev-release
  variables:
    IMAGE_REPO: $CI_REGISTRY_IMAGE/dev-releases
  script:
    - skopeo copy docker-archive:$IMAGE_TAR docker://$IMAGE_REPO:$IMAGE_VERSION


# todo: add multi-arch + manifest support
# https://github.com/containers/buildah/blob/main/docs/buildah-build.1.md
# https://github.com/containers/buildah/blob/main/docs/buildah-manifest.1.md
# https://medium.com/qiskit-openshift-multi-arch/building-and-publishing-multi-arch-images-and-image-manifests-with-red-hat-buildah-and-podman-927c717adaf3
# https://danmanners.com/posts/2022-01-buildah-multi-arch/
# todo: change docker build, slim, test, and eventually release, to be parallel/matrix and handle multi arch
# ARM/other arch support will have to wait until Gitlab.com supports other arch runners for use as SaaS.

# todo: add image signing (https://docs.sigstore.dev/) once they support gitlab/job-tokens as an identity.